package pageObjectes;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    final String MAIN_URL = "https://www.phptravels.net ";
    protected WebDriver driver;
    protected WebDriverWait wait;

    @FindBy(linkText = "Sign in")
    WebElement signInButton;

    @FindBy(className = "logout")
    public WebElement signOutButton;

    @FindBy(id = "search_query_top")
    WebElement searchInputField;

    @FindBy(xpath = "//a[@title='Women']")
    WebElement goToDressesButton;

public BasePage(WebDriver driver, WebDriverWait wait) {
    this.driver = driver;
    this.wait = wait;
    PageFactory.initElements(driver, this);
}

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(MAIN_URL);
    }

   public LoginPage goToLogin() {
        wait.until(ExpectedConditions.elementToBeClickable(signInButton));
        signInButton.click();
        return new LoginPage(driver, wait);
    }
/*
    public boolean isSignOutButtonDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(signOutButton));
        return signOutButton.isDisplayed();
    }

    public boolean isSignInButtonDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(signInButton));
        return signInButton.isDisplayed();
    }

    public void clickOnSignOutButton() {
        wait.until(ExpectedConditions.elementToBeClickable(signOutButton));
        signOutButton.click();
    }

    public SearchResultsPage searchFor(String searchPhrase) {
        wait.until(ExpectedConditions.elementToBeClickable(searchInputField));
        searchInputField.sendKeys(searchPhrase);
        searchInputField.sendKeys(Keys.ENTER);
        return new SearchResultsPage(driver, wait);
    }

    public ShoppingPage goShoppingPage() {

        wait.until(ExpectedConditions.elementToBeClickable(goToDressesButton));
        goToDressesButton.click();
        return new ShoppingPage(driver, wait);
    }*/
}
