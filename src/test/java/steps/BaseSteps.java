package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjectes.BasePage;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BaseSteps {

    protected BasePage basePage;
    protected WebDriver driver = null;
    protected WebDriverWait wait;


    @Before
    public void setUp() throws Exception {
        //driver = new ChromeDriver();
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 5);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
}
  /*  @After
    public void tearDown() throws IOException {
        takeScreenShot(driver);
        driver.quit();*/
  /*  }

    private void takeScreenShot(WebDriver driver) {
//        TODO implement correct takeScreenShot method that will save screenshot with unique name
        System.out.println("takeScreenShot method not implemented!");
    }

    @Given("Open homepage")
    public void open_homepage() {
        basePage = new BasePage(driver, wait);
        basePage.open();
    }

    @And("Go to login page")
    public void goToLoginPage() {
        loginPage = basePage.goToLogin();
    }

    @Given("Open login page")
    public void openLoginPage() {
        basePage = new BasePage(driver, wait);
        basePage.open();
        loginPage = basePage.goToLogin();
    }

    @When("Logout button is clicked")
    public void logoutButtonIsClicked() {
        basePage.clickOnSignOutButton();
    }
}
*/
